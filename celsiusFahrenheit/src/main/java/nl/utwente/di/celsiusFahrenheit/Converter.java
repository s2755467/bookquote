package nl.utwente.di.celsiusFahrenheit;

public class Converter {
    public double getConversion(String celsius) {
        double temperature = Double.parseDouble(celsius);
        return temperature * 1.8 + 32;
    }
}
