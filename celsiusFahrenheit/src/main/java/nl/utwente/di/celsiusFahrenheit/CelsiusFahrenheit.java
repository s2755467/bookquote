package nl.utwente.di.celsiusFahrenheit;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class CelsiusFahrenheit extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Converter converter;

    public void init() throws ServletException {
        converter = new Converter();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature converter";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celsius: " +
                request.getParameter("celsius") + "\n" +
                "  <P>Temperature in Fahrenheit: " +
                Double.toString(converter.getConversion(request.getParameter("celsius"))) +
                "</BODY></HTML>");
    }
}
