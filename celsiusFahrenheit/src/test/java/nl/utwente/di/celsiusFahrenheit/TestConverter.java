package nl.utwente.di.celsiusFahrenheit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestConverter {

    @Test
    public void test10Celsius() throws Exception {
        Converter converter = new Converter();
        double celsius = converter.getConversion("10");
        Assertions.assertEquals(50.0, celsius, 0.0, "10 celsius in fahrenheit");
    }
}
